#![no_std]
#![no_main]

// pick a panicking behavior
extern crate panic_halt; // you can put a breakpoint on `rust_begin_unwind` to catch panics
// extern crate panic_abort; // requires nightly
// extern crate panic_itm; // logs messages over ITM; requires ITM support
// extern crate panic_semihosting; // logs messages to the host stderr; requires a debugger

use nb::block;

use stm32f1xx_hal as hal;
use stm32f1xx_hal::{
    prelude::*,
    pac,
};

use cortex_m_rt::entry;
use embedded_hal;
use core::fmt::Write;
use max31855;

use st7735_embedded::{ST7735, St7735PixelColor};

pub struct Writer<SerialTx>
    where SerialTx: embedded_hal::serial::Write<u8>
{
    serial: SerialTx
}

impl<SerialTx> Writer<SerialTx>
    where SerialTx: embedded_hal::serial::Write<u8> {
    fn new(serial: SerialTx) -> Self {
        let w = Writer{ serial };
        w
    }
}

impl<SerialTx> core::fmt::Write for Writer<SerialTx>
    where SerialTx: embedded_hal::serial::Write<u8>
{
    fn write_str(&mut self, s: &str) -> core::fmt::Result {
        for byte in s.bytes() {
         block!(self.serial.write(byte)).ok();
        }
        Ok(())
    }
}

//PC13 low = led on
#[entry]
fn main() -> ! {
    let c = cortex_m::Peripherals::take().unwrap();
    let p = pac::Peripherals::take().unwrap();

    let mut rcc = p.RCC.constrain();
    let mut flash = p.FLASH.constrain();
    let clocks = rcc.cfgr.freeze(&mut flash.acr);
    let mut gpioc = p.GPIOC.split(&mut rcc.apb2);
    let mut delay = hal::delay::Delay::new(c.SYST, clocks);
    let mut afio = p.AFIO.constrain(&mut rcc.apb2);
    let mut gpiob = p.GPIOB.split(&mut rcc.apb2);
    let mut gpioa = p.GPIOA.split(&mut rcc.apb2);

    let spi_sck_pin = gpioa.pa5.into_alternate_push_pull(&mut gpioa.crl);
    let spi_miso_pin = gpioa.pa6;
    let spi_mosi_pin = gpioa.pa7.into_alternate_push_pull(&mut gpioa.crl);
    let spi_mode = hal::spi::Mode {
        polarity: hal::spi::Polarity::IdleLow,
        phase: hal::spi::Phase::CaptureOnFirstTransition
    };

    let mut spi = hal::spi::Spi::spi1(
        p.SPI1,
        (spi_sck_pin, spi_miso_pin, spi_mosi_pin),
        &mut afio.mapr,
        spi_mode,
        100.khz(),
        clocks,
        &mut rcc.apb2

        );

    let display_cs_pin = gpioa.pa3.into_push_pull_output(&mut gpioa.crl);
    let display_data_command_pin = gpioa.pa2.into_push_pull_output(&mut gpioa.crl);;
    let display_reset_pin = gpioa.pa0.into_push_pull_output(&mut gpioa.crl);;
    let mut display_backlight_pin = gpiob.pb0.into_push_pull_output(&mut gpiob.crl);
    display_backlight_pin.set_high();


    let mut display = ST7735::new(spi,
                                  display_cs_pin,
                                  display_data_command_pin,
                                  display_reset_pin,
                                  );

    display.hard_reset(&mut delay);
    display.init(&mut delay);

    let mut temperature_cs_pin =
        gpioa.pa4.into_push_pull_output_with_state(&mut gpioa.crl, stm32f1xx_hal::gpio::State::Low);
    //let mut max31855 = max31855::Max31855::new(spi, temperature_cs_pin).unwrap();

    let uart_tx_pin = gpiob.pb10.into_alternate_push_pull(&mut gpiob.crh);
    let uart_rx_pin = gpiob.pb11;

    let serial = hal::serial::Serial::usart3(
        p.USART3,
        (uart_tx_pin, uart_rx_pin),
        &mut afio.mapr,
        9600.bps(),
        clocks,
        &mut rcc.apb1
        );

    let (uart_tx, mut _uart_rx) = serial.split();
    let mut uart_writer = Writer::new(uart_tx);

    uart_writer.write_str("initialization complete!\n").unwrap();

    let mut led_pin = gpioc.pc13.into_push_pull_output(&mut gpioc.crh);
    let mut x: u8 = 0;
    let mut y: u8 = 0;
    let mut spi_buffer: [u8; 4] = [0; 4];
    let pixel =  St7735PixelColor {r: 255, b: 0, g: 0};
    loop {
        led_pin.set_high();
        //match max31855.read_thermocouple(max31855::Units::Fahrenheit) {
        //    Ok(t) => writeln!(uart_writer, "temperature: {} F", t).unwrap(),
        //    Err(e) => writeln!(uart_writer, "error: {:?}", e).unwrap(),
        //};

        delay.delay_ms(500_u16);
        x += 1;
        if x > 50 {
            x = 0;
            y += 1;
            if y > 50 {
                y = 0
            }
        }
        display.write_pixel(x, y, pixel);
        led_pin.set_low();
        delay.delay_ms(500_u16);
    }
}

